#!/usr/bin/env python3

import numpy as np

import inertia_tensors
import json_parser

def get_center_of_mass(objects: list[inertia_tensors.Object]) -> np.ndarray:
    """
    Return the position of the center of mass of the combination of all the 3d shape
    """

    # The center of mass of a system of particles is defined as the average of their positions, weighted by their masses
    com = np.zeros(3, dtype=float)
    total_mass = 0

    for o in objects:
        com += o.m * o.pose[0:3, 3]
        total_mass = o.m

    return com / total_mass

def main() -> None:
    """
    Main function
    """

    objects = json_parser.load("test.json")
    com = get_center_of_mass(objects)
    print(com)

if __name__ == '__main__':
    main()
