#!/usr/bin/env python3

from dataclasses import dataclass
import numpy as np

@dataclass
class Object:
    """
    Generic 3D object
    """

    pose: np.ndarray
    m: float

    def get_inertia_tensor(self) -> np.ndarray:
        """
        Return the inertia tensor of the object
        """

        return np.eye(3, dtype=float)

@dataclass
class SolidSphere(Object):
    """
    Solid sphere of radius r and mass m
    """

    r: float

    def get_inertia_tensor(self) -> np.ndarray:
        """
        Return the inertia tensor of the object
        """

        return np.array([[(2/5) * self.m * self.r**2, 0, 0], [0, (2/5) * self.m * self.r**2, 0], [0, 0, (2/5) * self.m * self.r ** 2]], dtype=float)

@dataclass
class SolidCuboid(Object):
    """
    Solid cuboid of width w, height h, depth d and mass m
    """

    w: float
    h: float
    d: float


    def get_inertia_tensor(self) -> np.ndarray:
        """
        Return the inertia tensor of the object
        """

        return np.array([[(1/12) * self.m * (self.h**2 + self.d**2), 0, 0], [0, (1/12) * self.m * (self.h**2 + self.d**2), 0], [0, 0, (1/12) * self.m * (self.h**2 + self.d**2)]], dtype=float)

@dataclass
class SolidCylinder(Object):
    """
    Solid cylinder of radius r, height h and mass m
    """

    r: float
    h: float

    def get_inertia_tensor(self) -> np.ndarray:
        """
        Return the inertia tensor of the object
        """

        return np.array([[(1/12) * self.m * (3 * self.r**2 + self.h**2), 0, 0], [0, (1/12) * self.m * (3 * self.r**2 + self.h**2), 0], [0, 0, (1/12) * self.m * (3 * self.r**2 + self.h**2)]], dtype=float)
