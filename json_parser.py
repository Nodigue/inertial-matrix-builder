#!/usr/bin/env python3

from pathlib import Path
import json

import numpy as np

import inertia_tensors

def read_json(path : Path) -> list | dict:
    """
    Read a json file and return its contents a dict or a list of dict
    """

    with path.open("r") as f:
        raw_data = f.read()
        json_data = json.loads(raw_data)

    return json_data

def check_fields(json_data: dict, field_list : list[str]) -> None:
    """
    Check if the specified fields are in the json_data
    """

    for field in field_list:
        if field not in json_data:
            raise RuntimeError(f'{field} field missing in json_data')

def parse_json_data(json_data : dict) -> inertia_tensors.Object:
    """
    Parse the json data and return the corresponding 3d object
    """

    check_fields(json_data, ['object', 'pose', 'mass'])

    object_str = str(json_data['object'])
    pose = np.array(json_data['pose'], dtype=float)
    m = float(json_data['mass'])

    if object_str == 'sphere':
        check_fields(json_data, ['radius'])

        r = float(json_data['radius'])

        return inertia_tensors.SolidSphere(pose, m, r)

    elif object_str == 'cube':
        check_fields(json_data, ['width', 'height', 'depth'])

        w = float(json_data['float'])
        h = float(json_data['height'])
        d = float(json_data['depth'])

        return inertia_tensors.SolidCuboid(pose, m, w, h, d)

    elif object_str == 'cylinder':
        check_fields(json_data, ['radius', 'height'])

        r = float(json_data['radius'])
        h = float(json_data['height'])

        return inertia_tensors.SolidCylinder(pose, m, r, h)

    else:
        raise RuntimeError(f'Invalid object.. Got {object_str}')

def load(path : str | Path) -> list[inertia_tensors.Object]:
    """
    Load a list of 3D objects from a json file
    """

    path = Path(path)

    json_data = read_json(path)

    objects = []
    for data in json_data:
        objects.append(parse_json_data(data))

    return objects
